from skeletons.ServerSkeleton import ServerSkeleton


def main():
    skeleton = ServerSkeleton()
    skeleton.run()


main()
