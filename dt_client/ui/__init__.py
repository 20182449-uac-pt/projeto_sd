# Control keys:
#       Down - Drop stone faster
# Left/Right - Move stone
#         Up - Rotate Stone clockwise
#     Escape - Quit game
#          P - Pause game
#     Return - Instant drop

QUIT = '<Escape>'
MOVE_LEFT = '<Left>'
MOVE_RIGHT = '<Right>'
MOVE_DOWN = '<Down>'
ROTATE_STONE = '<Up>'
TOGGLE_PAUSE = 'P'
PLAY_AGAIN = '<space>'
RETURN = '<Return>'

